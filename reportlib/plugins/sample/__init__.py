from datetime import datetime
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search, Q
from elasticsearch_dsl.query import Match
from prometheus_client import Counter


BASE_LABELS = {
    'customer': 'My customer label'
}

BASE_QUERIES = [
    {
        'type': 'bool',
        'args': {
            'minimum_should_match': 1,
            'should': [
                {
                    'match_phrase': {
                        'serverip': '10.0.0.10'
                    }
                },
                {
                    'match_phrase': {
                        'serverip': '10.0.0.11'
                    }
                }
            ],
        }
    },
]

FILTER_QUERIES = [
    {
        'type': 'range',
        'args': {
            'receivedtime': {
                'gte': 'now-5m',
                'lt': 'now'
            }
        }
    }
]

class ExportPlugin(object):

    def __init__(self):
        self.client = Elasticsearch()

        self.index_name = 'halon-{date}'.format(
            date=datetime.now().strftime('%Y-%m-%d')
        )

        self.reject_total = Counter(
            'skane_reject_total',
            'total rejected e-mails'
        )


    def update(self):
        s = Search(using=self.client, index=self.index_name)

        for base_q in BASE_QUERIES:
            s = s.query(base_q['type'], **base_q['args'])

        match_q = Q(
            'match',
            reason={
                'query': 'Rejected by spam filter',
            },
        )

        s = s.query(match_q)

        for filter_q in FILTER_QUERIES:
            s = s.filter(filter_q['type'], **filter_q['args'])

        response = s.execute()
        total_count = s.count()

        self.reject_total.inc(total_count)
