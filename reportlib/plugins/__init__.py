from importlib import import_module
from pkgutil import iter_modules
from configparser import NoSectionError

from reportlib import APP_NAME

class PluginManager(object):

    def __init__(self, **kw):
        config = kw.get('config')
        plugin_paths = [
            x.strip() for x in config.get(APP_NAME, 'plugin_paths').split(',')
        ]
        self.config = config
        self.plugins = {}
        for plugin_path in plugin_paths:
            self.plugins.update(
                self._find_plugins(import_module(plugin_path))
            )


    def _find_plugins(self, ns):
        ret_data = {}
        for f, name, ispkg in iter_modules(ns.__path__, ns.__name__+'.'):
            if not ispkg:
                continue
            if not self._is_enabled(name.split('.')[-1]):
                continue
            ret_data.update({
                name: {
                    'plugin_ns': import_module(name),
                    'name': name
                }
            })
        return ret_data


    def _is_enabled(self, config_section):
        try:
            is_enabled = self.config.getboolean(
                config_section,
                'enabled'
            )
        except NoSectionError:
            return False
        return self.config.getboolean(config_section, 'enabled')
