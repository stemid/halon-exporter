from configparser import RawConfigParser

try:
    from dotenv import load_dotenv
    load_dotenv(verbose=False)
except ImportError:
    pass

import click
from prometheus_client.twisted import MetricsResource
from twisted.web.server import Site
from twisted.web.resource import Resource
from twisted.internet import reactor
from twisted.internet.task import LoopingCall

from reportlib import APP_NAME
from reportlib.logger import AppLogger
from reportlib.plugins import PluginManager


class ServerDaemon(object):

    def __init__(self, **kw):
        self.listen_port = kw.get('listen_port', 3030)
        self.listen_host = kw.get('listen_host', 'localhost')

        self.config = RawConfigParser()
        self.config.read_file(kw.get('config'))

        al = AppLogger(name=APP_NAME, config=self.config)
        self.l = al.logger

        self._updates = []


    def run(self):
        pm = PluginManager(config=self.config)
        for name, data in pm.plugins.items():
            plugin = data.get('plugin_ns')
            self.l.info('Loading plugin:{plugin}'.format(
                plugin=name
            ))

            try:
                p = plugin.ExportPlugin()
            except Exception as e:
                self.l.error('Failed loading plugin:{plugin}: {error}'.format(
                    plugin=name,
                    error=str(e)
                ))
                continue

            self._updates.append({
                'plugin': name,
                'update_func': p.update
            })

            loop = LoopingCall(
                self.update
            )
            loop.start(10)

        root = Resource()
        root.putChild(b'metrics', MetricsResource())

        factory = Site(root)
        reactor.listenTCP(
            port=self.listen_port,
            interface=self.listen_host,
            factory=factory
        )

        base_url = 'http://{hostname}:{port}'.format(
            hostname=self.listen_host,
            port=self.listen_port
        )

        self.l.info('Serving metrics on {base_url}/metrics'.format(
            base_url=base_url
        ))

        reactor.run()


    def update(self):
        for u in self._updates:
            self.l.debug('Updating metrics for plugin:{plugin}'.format(
                plugin=u.get('plugin', 'UNKNOWN')
            ))
            try:
                u.get('update_func')()
            except Exception as e:
                self.l.error('Failed for plugin:{plugin}: {error}'.format(
                    plugin=u.get('plugin', 'UNKNOWN'),
                    error=str(e)
                ))
                continue


@click.command()
@click.option(
    '--config',
    type=click.File('r'),
    default=open('default.cfg', 'r')
)
@click.option(
    '--port', 'listen_port',
    default=3030,
)
@click.argument(
    'listen_host',
)
def run_daemon(**kw):
    daemon = ServerDaemon(
        **kw
    )
    daemon.run()

if __name__ == '__main__':
    exit(run_daemon(None, None, None))
