import sys

from logging import StreamHandler
from logging import Formatter, getLogger, getLevelName, DEBUG, WARN, INFO
from logging.handlers import SysLogHandler, RotatingFileHandler

# Mostly for documentation purposes
config_defaults = {
    'log_format': '%(asctime)s %(name)s[%(process)s] %(levelname)s: %(message)s',
    'log_debug': True,
    'log_level': 'INFO',
    'log_handler': 'stdout',
    'syslog_address': '/dev/log',
    'syslog_port': 514,
    'log_max_bytes': 20971520,
    'log_max_copies': 5,
    'log_file': './default.log'
}


class AppLogger(object):

    def __init__(self, name, config):
        """This initializes with a RawConfigParser object to get most of its
        default settings. 
        """

        formatter = Formatter(config.get('logging', 'log_format'))
        self.logger = getLogger(name)

        if config.get('logging', 'log_handler') == 'syslog':
            syslog_address = config.get('logging', 'syslog_address')
            default_facility = SysLogHandler.LOG_LOCAL0

            if syslog_address.startswith('/'):
                h = SysLogHandler(
                    address=syslog_address,
                    facility=default_facility
                )
            else:
                h = SysLogHandler(
                    address=(
                        config.get('logging', 'syslog_address'),
                        config.get('logging', 'syslog_port')
                    ),
                    facility=default_facility
                )
        elif config.get('logging', 'log_handler') == 'file':
            h = RotatingFileHandler(
                config.get('logging', 'log_file'),
                maxBytes=config.getint('logging', 'log_max_bytes'),
                backupCount=config.getint('logging', 'log_max_copies')
            )
        else:
            h = StreamHandler(stream=sys.stdout)

        h.setFormatter(formatter)
        self.logger.addHandler(h)

        try:
            log_level = getLevelName(
                config.get('logging', 'log_level').toupper()
            )
        except:
            log_level = DEBUG
            pass

        self.log_level = log_level
        self.handler = h
        self.logger.setLevel(log_level)


    def apply_handler(self, logger_name):
        """
        Some frameworks init their own loggers, like tornado for example.
        So use this to apply your handler to their loggers by specifying the
        logger name.
        """
        logger = getLogger(logger_name)
        logger.addHandler(self.handler)

