#!/usr/bin/env python3

from setuptools import setup, find_packages

setup_kwargs = {
    'name': 'halon-exporter',
    'version': '0.0.1',
    'description': 'Prometheus Halon exporter',
    'author': 'Stefan Midjich',
    'author_email': 'swehack@gmail.com',
    'url': 'https://gitlab.com/stemid/halon-exporter',
    'license': 'GPL',
    #'zip_safe': False, # This causes pickle to fail for rq
    'packages': [
        'reportlib'
    ],
    'entry_points': {
        'console_scripts': [
            'exporterd = reportlib.server:run_daemon'
        ]
    },
    'scripts': [],
    'data_files': [],
    'options': {},
}

setup(**setup_kwargs)
