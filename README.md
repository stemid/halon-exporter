# Halon Prometheus Exporter

## History

This project was exported from an internal Gitlab, it was originally written back in november 2019 to export Halon metrics from Elasticsearch into Prometheus. I publish it on Gitlab with the hopes of perhaps helping someone write a simple prometheus exporter in Python.

It originally had a private internal plugin under reportlib/plugins that is not included, the sample plugin is meant to be a good jumping off point for your own environment specific plugin.

Note the ``[rs]`` block in default.cfg which is for the missing internal plugin, replace that with your own plugin.

## Technical design

* Plugins are installed into ``reportlib/plugins`` as python modules.
* ``default.cfg`` toggles plugins by having an ini config section for each plugin, named after the plugin folder.
* ``reportlib/server.py`` loads and imports all the plugin modules that are enabled in the config file.
* It expects to find an ExportPlugin class based on the example in the sample plugin.
* It then expects ti run ExportPlugin.update() method periodically to refresh metrics from the source.
* And finally publishes those metrics through the ``prometheus_client`` python library.
* All this is run using a Twisted.internet.reactor TCP server to serve the metrics.

## Install

### Virtualenv pip packages

  python3 -m venv .venv
  source .venv/bin/activate
  pip install -r requirements.txt

## Run in virtualenv

  python -m reportlib.server --help
  python -m reportlib.server localhost
